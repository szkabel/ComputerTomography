function [ paramcell ] = fetchUIControlValues( paramarray , paramEditHandles, checkHandle)
% AUTHOR:   Abel Szkalisity
% DATE:     June 30, 2017
% NAME:     createClassImage
%
% CUSTOMIZED FOR CT COURSE
%
% This function extracts the parameters from uicontrol elements and returns
% them in a cellarray. If the given parameter in the control field does not
% fulfil the condition implied by the corresponding paramarray type
% (currently we only check that the 'int' typed input field must be a
% number) then we show a warning message to the user. In that case the
% paramcell output is unreliable.
%
% INPUT:
%   paramarray      See generateUIControls.
%   paramEditHandles A cellarray of handles to the uicontrol objects. We
%                   suppose that the GUI was generated with the
%                   generateUIControl function from the paramarray
%                   specification therefore paramarray and paramEditHandles
%                   has equal length. The type of the ith entry of the
%                   paramEditHandles is determined by the ith entry of the
%                   paramarray. (e.g. for enum type parameter there is a
%                   popuplist graphical object)
%   checkHandle     A handle to a function (which can be the string with
%                   the function name), that is going to be called with one
%                   parameter the potential paramcell output. This
%                   parameter is OPTIONAL. If it is provided then the int
%                   typed parameters are not checked to be number the only
%                   parameter check that is run is the given function. The
%                   function must return a boolean value and a message
%                   (string) to display that describes the problem if the
%                   boolean is not 1.
%
% OUTPUT:
%   paramcell       A cellarray with the fetched parameters. The type of
%                   the ith entry of this is determined by the type of the
%                   corresponding paramarray. (e.g. for 'int' type it is a
%                   numeric value (can be matrix though)). The length is
%                   equal to paramarray's length. For slider it is a
%                   number, for checkbox is 0/1, for colorPicker is an
%                   array (RGB)
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


nofParams = length(paramarray);
paramcell = cell(1,nofParams);
ok = 1;

for i=1:length(paramEditHandles)
    if strcmp(paramarray{i}.type,'str')
        paramcell{i} = get(paramEditHandles{i},'string');
    elseif strcmp(paramarray{i}.type,'int')
        paramcell{i} = str2num(get(paramEditHandles{i},'string')); %#ok<ST2NM> It can happen that we need convert into array.        
        %check for number, not for int.
        if nargin<3 && (isempty(paramcell{i}))
            msg = ['Parameter ''' paramarray{i}.name ''' has to be a number!'];
            ok = 0;
            break;
        end
    elseif strcmp(paramarray{i}.type,'enum')
        paramcell{i} = get(paramEditHandles{i},'value');
    elseif strcmp(paramarray{i}.type,'slider') || strcmp(paramarray{i}.type,'checkbox')        
        paramcell{i} = get(paramEditHandles{i},'Value');
    elseif strcmp(paramarray{i}.type,'colorPicker')
        paramcell{i} = get(paramEditHandles{i},'BackgroundColor');
    end
end

if nargin>=3
    [ok,msg] = feval(checkHandle,paramcell);
end

if ~ok
    errordlg(msg,'Parameter error');
    %throw error unreliable output
    error('ACC:errorConstraintFault',msg);
end

end