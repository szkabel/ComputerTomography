function [ x ] = DRTReconstruct()

nofTrials = 10;
solutions = cell(1,nofTrials);
funcValues = zeros(1,nofTrials);

default = saoptimset('simulannealbnd');
default.AnnealingFcn = @xnew;
default.MaxFunEvals = 10000;
default.TolFun = 10;
default.StallIterLimit = 100;

global globOptions;

for i=1:nofTrials
    [solutions{i},funcValues(i)] = simulannealbnd(@fun,round(rand(globOptions.imageSize)),0,1,default);    
end

[~,minIdx] = min(funcValues);

x = solutions{minIdx};

end

