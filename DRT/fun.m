function objValue = fun(x)
%the single parameter is the image itself
%this function measures the difference from the image to be reconstructed.
%   The original image's sinogram is stored in a global variable and we
%   extract it from there.
%   We also need the parameters of the radon transform (angles of the projection)
%   These 2 parameters are extracted from the global variable globOptions
%   x is binary

global globOptions; %struct with fields b and theta

Px = radon(x,globOptions.theta);
bFlattened = globOptions.b(:);
objValue = norm(Px(:)-bFlattened).^2 - g(x);

end

function v = g(x)
    convMask = [0 1 0; 1 0 1; 0 1 0]; %mask to count pixels with value 1 in 4 neghbourhood
    counts = conv2(x,convMask,'same');
    v = sum(sum(counts.*x));    
end