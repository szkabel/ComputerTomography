function [ results ] = RunDRTTests( testImages )
%RunDTTests runs DRTReconstruct() on each testImages, 
%   and sets global variables. In the results structure goes 
%   reconstructed Images and relative mean errors to each input image and parameter combinaions. 
%   we used default settings for the simulated annealing mostly, except for
%   the number of function evaluations, that we set to 10000.
%   
%   The results array is similar to the one in RunTests. Contains the
%   scale, the parameters, the reconstructed images and the error
%   measurements.

nofScales = length(testImages);
results = cell(1,nofScales);

for j=1:nofScales

    h = waitbar(0,sprintf('Running tests on scale %d... %d %%',testImages{j}.scale,0));    
    
    parameters.projections = 5:5:40;
    parameters.noise = [0, 0.001, 0.01, 0.1, 1];

    dimNames = fieldnames(parameters);
    dims = zeros(1,length(dimNames)+1);
    dims(1) = length(testImages{j}.images);
    for ii = 1:length(dimNames)
        dims(1+ii) = length(parameters.(dimNames{ii}));
    end

    results{j}.parameters = parameters;
    results{j}.errorMeasures = zeros(dims);
    results{j}.reconstructions = cell(dims);
    
    for i=1:dims(1)
        for ii=1:dims(2)
            for iii =1:dims(3)
                img = imbinarize(testImages{j}.images{i}); 
                results{j}.reconstructions{i,ii,iii}...
                    = performDiscretely(...
                    img,...
                    parameters.projections(ii),...
                    parameters.noise(iii));
                    results{j}.errorMeasures(i,ii,iii) = RMR(img,results{j}.reconstructions{i,ii,iii});
                percent = ((i-1)*prod(dims(2:3))+(ii-1)*dims(3)+iii)/prod(dims(1:3))*100;
                waitbar(percent/100,h,sprintf('Running tests on scale %d... %d %%',testImages{j}.scale,floor(percent)));
            end
        end
        save('DiscreteResults.mat','results');
    end            
    if ishandle(h), close(h); end
end

end

function reconstruction = performDiscretely(inputImage, nofProjections, noise)
    theta = linspace(0,179,nofProjections);
    R = radon(inputImage,theta);
    if noise ~=0
        R = R + randn(size(R)).*noise;
    end
    
    global globOptions;
    
    globOptions.b = R;
    globOptions.theta = theta;
    globOptions.imageSize = size(inputImage);
    
    reconstruction = DRTReconstruct();
end

