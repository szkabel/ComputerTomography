function [ scale,image,projection,interpolation,filter,noise ] = getParametersFromPanel( handles )

scale =         get(handles.popupScale,'Value');
image =         get(handles.popupImage,'Value');
projection =    get(handles.popupProjection,'Value');
interpolation = get(handles.popupInterpolation,'Value');
filter =        get(handles.popupFilter,'Value');
noise =         get(handles.popupNoise,'Value');

end

