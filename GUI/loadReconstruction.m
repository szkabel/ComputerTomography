function loadReconstruction( hObject,~ )

handles = guidata(hObject);

uData = get(handles.figure1,'UserData');

paramcell = fetchUIControlValues( uData.origParams , uData.origParamEditHandles);
scale = paramcell{1};
imageNumber = paramcell{2};

imshow(imbinarize(uData.testImages{scale}.images{imageNumber}),'Parent',handles.axesOriginalImage);

if ~isempty(uData.contResults)
    paramcell = fetchUIControlValues( uData.contParams , uData.contParamEditHandles);
    imshow(imbinarize(uData.contResults{scale}.reconstructions{imageNumber,paramcell{1},paramcell{2},paramcell{3},paramcell{4}}),'Parent',handles.axesReconstruction);
end

if ~isempty(uData.discResults)
    paramcell = fetchUIControlValues( uData.discParams , uData.discParamEditHandles);
    imshow(uData.discResults{scale}.reconstructions{imageNumber,paramcell{1},paramcell{2}},'Parent',handles.axesDiscreteReconstruction);
end



end

