function plotHighDdata( dataToPlot, pars, parameters, selectedAxis, selectedMeanAxis)
%dataToPlot can be a high D matrix from which we select 1 or 2 axis to be
%plotted.
%In principle pars is as long as as many dimension dataToPlot has. That is
%the selected intersection of the matrix. However on the selectedAxis axes
%the data will be displayed for the whole matrix, and for the
%selectedMeanAxis it will be averaged.
%parameters is a structure again as long as as many dimensions dataToPlot
%has, and it stores the actual values of the parameters. The oreder of the
%fieldnames is the same as the order of the dimensions.
%The entries of this cellarray can be cellarray or
%in case of numeric data a matrix.


for i=1:length(pars)
    if ismember(i,selectedMeanAxis)
        dataToPlot = mean(dataToPlot,selectedMeanAxis);
    elseif ~ismember(i,selectedAxis)
        str = repmat(':,',1,i-1);
        str = [str num2str(pars(i))];
        str = [str repmat(',:',1,length(pars)-i)];
        eval(['dataToPlot = dataToPlot(' str ');']);        
    end
end

if length(selectedAxis) == 2
     M = zeros(size(dataToPlot,selectedAxis(1)),size(dataToPlot,selectedAxis(2)));
     for i=1:size(M,1)
         for j=1:size(M,2)
             str = repmat(':,',1,selectedAxis(1)-1);
             str = [str num2str(i)];
             str = [str repmat(',:',1,selectedAxis(2)-selectedAxis(1)-1)];
             str = [str ',' num2str(j)];
             str = [str repmat(',:',1,length(pars)-selectedAxis(2))];
             eval(['M(i,j) = dataToPlot(' str ');']);
         end
     end
     fieldNames = fieldnames(parameters);
     if selectedAxis(1) == 1
         firstArray = 1:size(dataToPlot,1);
         firstTitle = 'Images';
     else         
         if iscell(parameters.(fieldNames{selectedAxis(1)-1}))
             firstArray = 1:length(parameters.(fieldNames{selectedAxis(1)-1}));
         else
            firstArray = parameters.(fieldNames{selectedAxis(1)-1});
         end
         firstTitle = fieldNames{selectedAxis(1)-1};
     end
     if iscell(parameters.(fieldNames{selectedAxis(2)-1}))
         secondArray = 1:length(parameters.(fieldNames{selectedAxis(2)-1}));
     else
         secondArray = parameters.(fieldNames{selectedAxis(2)-1});
     end
     secondTitle = fieldNames{selectedAxis(2)-1};
     
     [X,Y] = meshgrid(secondArray,firstArray);     
     f = figure;
     try
        surf(X,Y,M);
     catch
         errordlg('Something strange happened. Try to use for surface plot a parameter that has at least 2 different value!');
         if ishandle(f), close(f); end
     end
     xlabel(secondTitle);
     ylabel(firstTitle);
     zlabel('RMR');
     
     % {
     figure;
     [a,b] = closestDivisor(size(M,2));
     for j=1:size(M,2)
         subplot(a, b, j)    
         plot(firstArray, M(:,j));
         xlabel(firstTitle);
         title([secondTitle '-' num2str(secondArray(j))])
         ylabel('RMR'); 
     end          
    
     figure;     
     [a,b] = closestDivisor(size(M,1));
     for j=1:size(M,1)
         subplot(a,b,j);
         plot(secondArray, M(j,:));
         xlabel(secondTitle);
         title([firstTitle '-' num2str(firstArray(j))])
         ylabel('RMR');   
     end
     %}
end

if length(selectedAxis) == 1
     M = zeros(size(dataToPlot,selectedAxis(1)),1);
     for i=1:size(M,1)         
         str = repmat(':,',1,selectedAxis(1)-1);
         str = [str num2str(i)];
         str = [str repmat(',:',1,length(pars)-selectedAxis(1))];         
         eval(['M(i) = dataToPlot(' str ');']);
     end
     fieldNames = fieldnames(parameters);
     if selectedAxis(1) == 1
         firstArray = 1:size(dataToPlot,1);
         firstTitle = 'Images';
     else         
         if iscell(parameters.(fieldNames{selectedAxis(1)-1}))
             firstArray = 1:length(parameters.(fieldNames{selectedAxis(1)-1}));
         else
            firstArray = parameters.(fieldNames{selectedAxis(1)-1});
         end
         firstTitle = fieldNames{selectedAxis(1)-1};
     end
     figure;
     plot(firstArray,M);
     xlabel(firstTitle);
     ylabel('RMR');
end


end

