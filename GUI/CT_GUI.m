function varargout = CT_GUI(varargin)
% CT_GUI MATLAB code for CT_GUI.fig
%      CT_GUI, by itself, creates a new CT_GUI or raises the existing
%      singleton*.
%
%      H = CT_GUI returns the handle to a new CT_GUI or the handle to
%      the existing singleton*.
%
%      CT_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CT_GUI.M with the given input arguments.
%
%      CT_GUI('Property','Value',...) creates a new CT_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CT_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CT_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CT_GUI

% Last Modified by GUIDE v2.5 17-Nov-2017 00:15:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CT_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @CT_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CT_GUI is made visible.
function CT_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CT_GUI (see VARARGIN)
%
% This function needs to be called with 3 parameters:
%   The testImages array created by CreateTestImages
%   The result array for the FBP reconstruction created by runTests.
%   The result array for the discrete reconstruction created by
%   runDRTTTests.m

% Choose default command line output for CT_GUI
handles.output = hObject;

uData.testImages = varargin{1};
uData.contResults = varargin{2};
uData.discResults = varargin{3};

%fill up the GUI with parameters %original image GUI
scaleString = cell(1,length(uData.testImages));
for j=1:length(uData.testImages)
    scaleString{j} = uData.testImages{j}.scale;
end
 
paramarray{1} = struct('name','scale','type','enum','values',scaleString);
nofImages = length(uData.testImages{1}.images);
paramarray{2} = struct('name','images','type','enum','values',{num2cell(1:nofImages)});
[ uData.origParamTextHandles, uData.origParamEditHandles ] = generateUIControls( paramarray, handles.panelOriginal, 20, [5 45],'loadReconstruction');
uData.origParams = paramarray;

%Continuous reconstruction GUI
if ~isempty(uData.contResults)
    fn = fieldnames(uData.contResults{1}.parameters);
    paramarray = cell(1,length(fn));
    for i=1:length(paramarray)
        if iscell(uData.contResults{1}.parameters.(fn{i}));
            values = uData.contResults{1}.parameters.(fn{i});
        else
            values = num2cell(uData.contResults{1}.parameters.(fn{i}));
        end
        paramarray{i} = struct('name',fn{i},'type','enum','values',{values});
    end
    [ uData.contParamTextHandles, uData.contParamEditHandles ] = generateUIControls( paramarray, handles.panelFBP, 20, [5 45],'loadReconstruction');
    uData.contParams = paramarray;
end

%Discrete reconstruction GUI
if ~isempty(uData.discResults)
    fn = fieldnames(uData.discResults{1}.parameters);
    paramarray = cell(1,length(fn));
    for i=1:length(paramarray)
        if iscell(uData.discResults{1}.parameters.(fn{i}));
            values = uData.discResults{1}.parameters.(fn{i});
        else
            values = num2cell(uData.discResults{1}.parameters.(fn{i}));
        end
        paramarray{i} = struct('name',fn{i},'type','enum','values',{values});
    end
    [ uData.discParamTextHandles, uData.discParamEditHandles ] = generateUIControls( paramarray, handles.panelDiscrete, 20, [5 45],'loadReconstruction');
    uData.discParams = paramarray;
end


set(hObject,'UserData',uData);

% Update handles structure
guidata(hObject, handles);

loadReconstruction(hObject);

% UIWAIT makes CT_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CT_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Button_ErrorBar.
function Button_ErrorBar_Callback(hObject, eventdata, handles)
% hObject    handle to Button_ErrorBar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');

listStr = {'images'};
results = uData.contResults;
listStr = [listStr, fieldnames(results{1}.parameters)'];

[selectedAxis,isOk] = listdlg('PromptString','Select parameters to plot along: (max 2)',...
                'ListString',listStr);
if length(selectedAxis)>2 || length(selectedAxis)<1 || ~isOk
    msgbox('Please select at least 1 and at most 2 parameters!');
    return;
end
possibleMeanIndices = setdiff(1:length(listStr),selectedAxis);
            
[selectedMeanAxis,isOk] = listdlg('PromptString','Select parameters to take average on:',...
                'ListString',listStr(possibleMeanIndices));
selectedMeanAxis = possibleMeanIndices(selectedMeanAxis);
            
pars = [0,0,0,0,0];
paramcell = fetchUIControlValues( uData.origParams , uData.origParamEditHandles);
scale = paramcell{1};
pars(1) = paramcell{2};
paramcell = fetchUIControlValues( uData.contParams , uData.contParamEditHandles);
for j=1:4
    pars(j+1) = paramcell{j};
end

dataToPlot = results{scale}.errorMeasures;

plotHighDdata( dataToPlot, pars, results{scale}.parameters, selectedAxis, selectedMeanAxis);
                 

% --- Executes on button press in Button_discErrorBar.
function Button_discErrorBar_Callback(hObject, eventdata, handles)
% hObject    handle to Button_discErrorBar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');

listStr = {'images'};
results = uData.discResults;
listStr = [listStr, fieldnames(results{1}.parameters)'];

[selectedAxis,isOk] = listdlg('PromptString','Select parameters to plot along: (max 2)',...
                'ListString',listStr);
if length(selectedAxis)>2 || length(selectedAxis)<1 || ~isOk
    msgbox('Please select at least 1 and at most 2 parameters!');
    return;
end
possibleMeanIndices = setdiff(1:length(listStr),selectedAxis);
            
[selectedMeanAxis,isOk] = listdlg('PromptString','Select parameters to take average on:',...
                'ListString',listStr(possibleMeanIndices));
selectedMeanAxis = possibleMeanIndices(selectedMeanAxis);
            
pars = [0,0,0];
paramcell = fetchUIControlValues( uData.origParams , uData.origParamEditHandles);
scale = paramcell{1};
pars(1) = paramcell{2};
paramcell = fetchUIControlValues( uData.discParams , uData.discParamEditHandles);
for j=1:2
    pars(j+1) = paramcell{j};
end

dataToPlot = results{scale}.errorMeasures;

plotHighDdata( dataToPlot, pars, results{scale}.parameters, selectedAxis, selectedMeanAxis);
                 

