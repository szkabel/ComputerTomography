function [ results ] = RunTests( testImages)
%OUTPUT:
%   results is a cellarray exactly as long as testImages.
%   Each entry corresponds to an image scale, and contains a structure.
%   The fields of the structure are the following:
%       .scale: The size of the squared image
%       .parameters: This a structure with the following fields
%           .projections: an array with the projection numbers
%           .interpolations: a cellarray with the names of interpolation
%           methods used in the test
%           .filters: a cellarray with the names of the filters for the FBP
%           .noise: an array with the used Gaussian noise variances
%       .reconstructions A 5D cellarray for all the reconstructed images.
%       The dimensions correspond to:
%           1D: the images
%           2D: nof projections
%           3D: interpolations
%           4D: filters
%           5D: noise
%       .errorMeasures A 5D array similarly encoded as the reconstructions
%       that stores the reconstruction error defined by:
%           sum(|o_ij-r_ij|)/number of object pixels on the original image

nofScales = length(testImages);
results = cellarray(1,length(testImages));

rng(19920129);

for j=1:nofScales
    results{j}.scale = testImages{j}.scale;
    parameters.projections = [10 15 20 25 40 80 180];
    parameters.interpolations = {'linear','spline'};%,'pchip'};
    parameters.filters = {'Cosine','Shepp-Logan','Hamming'};%{'Shepp-Logan','Cosine','Hamming','Hann','None'}
    parameters.noise = [0, 0.001, 0.01, 0.1, 1];

    dimNames = fieldnames(parameters);
    dims = zeros(1,length(dimNames)+1);
    dims(1) = length(testImages{j}.images);
    for ii = 1:length(dimNames)
        dims(1+ii) = length(parameters.(dimNames{ii}));
    end

    results{j}.parameters = parameters;
    results{j}.errorMeasures = zeros(dims);
    results{j}.reconstructions = cell(dims);

    h = waitbar(0,sprintf('Running tests on scale %d... %d %%',testImages{j}.scale,0));
    
    %TODO: This CODE is ugly and needs revision
    for i = 1:dims(1)
        for ii = 1:dims(2)
            for iii=1:dims(3)
                for iv = 1:dims(4)
                    for v = 1:dims(5)
                        inputImg = imbinarize(testImages{j}.images{i});
                        results{j}.reconstructions{i,ii,iii,iv,v} = ...
                            performSingleTransform(...
                            inputImg,...
                            parameters.projections(ii),...
                            parameters.interpolations{iii},...
                            parameters.filters{iv},...
                            parameters.noise(v));
                        results{j}.errorMeasures(i,ii,iii,iv,v) = RMR(inputImg, imbinarize(results{j}.reconstructions{i,ii,iii,iv,v}));
                    end
                end
                percent = ((i-1)*prod(dims(2:3))+(ii-1)*dims(3)+iii)/prod(dims(1:3))*100;
                waitbar(percent/100,h,sprintf('Running tests on scale %d... %d %%',testImages{j}.scale,floor(percent)));
            end
        end
    end
    
    if ishandle(h), close(h); end

end
    
end

function reconstruction = performSingleTransform(inputImage,nofProjections,interpolation,filter,noise)
    theta = linspace(0,179,nofProjections);
    R = radon(inputImage,theta);
    if noise ~=0
        R = R + randn(size(R)).*noise;
    end
    reconstruction = iradon(R,theta,interpolation,filter, size(inputImage,1));

end
