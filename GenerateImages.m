function [ P ] = GenerateImages( n, nofEllipsis)
%INPUT
%OUTPUT

E = zeros(nofEllipsis+2,6);

scale = [0.6624 0.874];
E(1:2,:) = [1 0.69 0.92 0 0 0; ...
 -0.80 scale 0 -0.0184 0];

for i=1:nofEllipsis
    intensity = rand*0.5+0.25;
    lengths = rand(1,2)*0.25.*scale;
    centers = (rand(1,2)-0.5).*scale;
    alpha = rand*pi;
    E(2+i,:) = [intensity, lengths, centers, alpha];
end

P = phantom(E,n);

end

