firstArray = 5:5:40;
secondArray = [0, 0.001, 0.01, 0.1, 1];
secondTitle='Noise';
firstTitle='Number of Projections';
[X,Y]=meshgrid(secondArray,firstArray);
M = zeros(length(firstArray),length(secondArray));
for i=1:length(firstArray)
    for j=1:length(secondArray)
        M(i,j)=results{1}.errorMeasures(1,i,j);
    end
end
f=figure;
surf(X,Y,M);
xlabel(secondTitle);
ylabel(firstTitle);
zlabel('RMErr');