function [ x ] = xnew( s, ~)
x = s.x;
idx = ceil(rand * numel(x));
x(idx)=1-x(idx);
end

