function [ paramTextHandles, paramEditHandles ] = generateUIControls( paramarray, parentHandle, heightOfItems, margin, callback)
% AUTHOR:   Abel Szkalisity
% DATE:     Nov 17, 2017
% NAME:     generateUIControls
%
% To create the 'form' of any method on graphical user interface.
%
% |?^VM
% | <- HM -> PARAMETER1 <- HM -> || INPUT_FIELD1 <- HM ->*2 |
% |
% | <- HM -> PARAMETER2 <- HM -> || INPUT_FIELD2 <- HM ->*2 |
%
%   The most important outcome of the function is that the uicontrols are
%   placed on the parent control in a dinamic way according to paramarray.
%   However for further usage the handles to the uicontrols are also
%   returned.
%
% INPUT:
%   paramarray      a cellarrray with structures. Every structure describes
%                   one input parameter. This structure has the following
%                   fields:
%                   .name: the name of the parameter (this will be printed
%                   to the user when asked for the parameter value on the
%                   GUI)
%                   .type: This determines the type of the parameter.
%                       int
%                       enum Possible values listed in .values cellarray
%                       str
%                       slider Limits are listed in .lim
%                       checkbox
%                       
%   parentHandle    handle to the parent object to which we're going to add
%                   the uicontrols. It must have a position property.
%   heightOfItems   The height of each ui item and the distance between
%                   them as well. (in the drawing at the top this is the
%                   height of a row) 
%   margins         Array with two values: HM and VM (horizontal and
%                   vertical margin) in this order.
%   callback        The callback function handle
%
% OUTPUT:
%   paramTextHandles handles to the parameter names.
%   paramEditHandles handles to the input fields.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

nofParams = length(paramarray);

paramTextHandles = cell(1,nofParams);
paramEditHandles = cell(1,nofParams);

sizeOfPanel = get(parentHandle,'Position');

for i=1:nofParams
    paramTextHandles{i} = uicontrol('Parent',parentHandle,'Units','pixels','Style','Text','String',paramarray{i}.name,'Position',[margin(1)/2, sizeOfPanel(4)-margin(2)-(i-1)*heightOfItems*1.5, sizeOfPanel(3)/2-margin(1)*2 heightOfItems]);
    editPosition =  [sizeOfPanel(3)/2, sizeOfPanel(4)-margin(2)-(i-1)*heightOfItems*1.5, sizeOfPanel(3)/2-margin(1)*2 heightOfItems];
    switch paramarray{i}.type
        case 'int'
            paramEditHandles{i} = uicontrol('Parent',parentHandle,'Units','pixels','Style','edit','Position',editPosition,'BackgroundColor','white');
        case 'str'
            paramEditHandles{i} = uicontrol('Parent',parentHandle,'Units','pixels','Style','edit','Position',editPosition,'BackgroundColor','white');
        case 'enum'        
            paramEditHandles{i} = uicontrol('Parent',parentHandle,'Units','pixels','Style','popupmenu','Position',editPosition,'BackgroundColor','white','Callback',@(hObject,eventdata)feval(callback,hObject,eventdata));
            set(paramEditHandles{i},'String',paramarray{i}.values);
        case 'slider'
            paramEditHandles{i} = uicontrol('Parent',parentHandle,'Units','pixels','Style','slider','Position',editPosition,'BackgroundColor','white');
            set(paramEditHandles{i},'Min',paramarray{i}.lim(1));
            set(paramEditHandles{i},'Max',paramarray{i}.lim(2));
        case 'checkbox'
            paramEditHandles{i} = uicontrol('Parent',parentHandle,'Units','pixels','Style','checkbox','Position',editPosition);
        case 'colorPicker'
            paramEditHandles{i} = uicontrol('Parent',parentHandle,'Units','pixels','Style','pushbutton','Position',editPosition,'BackgroundColor','white','Callback',@(hObject,eventdata)colorPickerCallback(hObject,eventdata,guidata(hObject)));
    end
end
               

end

function colorPickerCallback(hObject,eventdata,handles)
    pickedColor = uisetcolor();
    if length(pickedColor)==3
        set(hObject,'BackgroundColor',pickedColor);
    end
       
end

