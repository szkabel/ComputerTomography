function [ TestImages ] = CreateTestImages( nOfImages, scales )
%OUTPUT: CellArray of structures corresponding to each scale

TestImages = cell(1,length(scales));

for i=1:length(scales)
    TestImages{i}.scale = scales(i);
    rng(19920129);
    for j=1:nOfImages
        TestImages{i}.images{j} = GenerateImages(scales(i),ceil(rand*6));
    end
end


end

