function fillOutUIControls( paramarray, paramValues,paramEditHandles )
% AUTHOR:   Abel Szkalisity
% DATE:     Dec 09, 2016
% NAME:     fillOutUIControls
%
% In cooperation with generateUIControls this function fills up UI fields
% with parameters. (e.g. this can be used to see the current settings of
% your method)
%
% INPUT:
%   paramarray      see generateUIControls function same input.
%   paramValues     A cellarray (exactly as long as paramarray and the
%                   matching positions refers to the same parameter) The
%                   type of the parameter value in this array is determined
%                   by the paramarray matching position.
%   paramEditHandles See the output of generateUIControls.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

for i=1:length(paramarray)
    switch paramarray{i}.type
        case 'int'
            set(paramEditHandles{i},'string',num2str(paramValues{i}));
        case 'str'
            set(paramEditHandles{i},'string',paramValues{i});
        case 'enum'                        
            for j=1:length(paramarray{i}.values)
                %TODO: handle cases when the enum is not string.
                if strcmp(paramValues{i},paramarray{i}.values{j});
                    break;
                end                    
            end
            set(paramEditHandles{i},'value',j);
        case 'checkbox'
            set(paramEditHandles{i},'value',paramValues{i});
        case 'slider'
            set(paramEditHandles{i},'value',paramValues{i});
        case 'colorPicker'
            set(paramEditHandles{i},'BackgroundColor',paramValues{i});
    end
end

